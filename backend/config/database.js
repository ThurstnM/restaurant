import mysql from "mysql2";

// create the connection to database

const db = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "restoapp_db"
});


db.connect(error => {
  if (error) throw error;
  console.log("Koneksi Ke Database Berhasil");
});

export default db;