import db from "../config/database.js";

// get all Foods
export const getMenus = (result) => {
    db.query("SELECT * FROM menu", (err,results)=> {
        if (err){
            console.log(err);
            result(err,null);
        }else{
            result(null,results);
        }
    });
};

// get single Foods
export const getMenuById = (id,result) => {
    db.query("SELECT * FROM menu WHERE menu_id = ?",[id], (err,results)=> {
        if (err){
            console.log(err);
            result(err,null);
        }else{
            result(null,results[0]);
        }
    });
};

// insert Food
export const insertMenu = (data,result) => {
    db.query("INSERT INTO menu SET ?",data, (err,results)=> {
        if (err){
            console.log(err);
            result(err,null);
        }else{
            result(null,results[0]);
        }
    });
};

// update Food
export const updateMenuById = (data,id,result) => {
    db.query("UPDATE food SET menu_name = ?, menu_price = ? WHERE menu_id = ?",[data.menu_name, data.menu_price, id], (err,results)=> {
        if (err){
            console.log(err);
            result(err,null);
        }else{
            result(null,results);
        }
    });
};


// delete Food
export const deleteMenuById = (id,result) => {
    db.query("DELETE FROM menu WHERE menu_id = ?",[id], (err,results)=> {
        if (err){
            console.log(err);
            result(err,null);
        }else{
            result(null,results);
        }
    });
};