const { defineConfig } = require('@vue/cli-service')
// const path = require('path');
module.exports = {
  devServer: {
    port: 8065
  },
},
  defineConfig({
    transpileDependencies: true
});

// module.exports = {
//   outputDir: path.resolve(__dirname, '../backend/public'),
//   devServer: {
//     proxy: {
//       '/': {
//         target: 'http://localhost:8081'
//       }
//     }
//   }
// }
