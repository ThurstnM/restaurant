import {createStore} from "vuex"
import axios from "axios"

const store = createStore({
    state() {
        return {
            allMenus: [],
            user: undefined,
            admin: undefined,
        }
    },
    mutations: {
        setMenusData(state, payload) {
            state.allMenus = payload;
        },
        setUser(state, payload) {
            state.user = payload;
        },
        setAdmin(state, payload) {
            state.admin = payload;
        }
    },
    actions: {
        async getMenusData(context) {
            await axios.get('/menus')
            .then(function (response) {
                context.commit("setMenusData", response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
        }
    }
})

export default store;